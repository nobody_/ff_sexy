user_pref("network.proxy.socks_remote_dns",true);
user_pref("network.dns.disablePrefetch",true);
user_pref("network.dns.disableIPv6",true);
user_pref("plugins.click_to_play",true);
user_pref("webgl.disabled",true);
user_pref("media.peerconnection.enabled",false);
user_pref("geo.enabled",false);
user_pref("geo.wifi.uri","https://127.0.0.1/");
user_pref("browser.send_pings",false);
user_pref("dom.storage.enabled",true);
user_pref("browser.search.suggest.enabled",false);
user_pref("security.OCSP.enabled","0");
user_pref("loop.enabled",false);
user_pref("browser.casting.enabled",false);
user_pref("network.predictor.enabled",false);
user_pref("browser.pocket.enabled",false);
user_pref("privacy.trackingprotection.enabled",false);
user_pref("browser.urlbar.formatting.enabled",false);
user_pref("browser.urlbar.trimURLs",false);
user_pref("browser.safebrowsing.enabled",false);
user_pref("browser.safebrowsing.malware.enabled",false);
user_pref("dom.network.enabled",false);
user_pref("datareporting.healthreport.service.enabled",false);
user_pref("datareporting.healthreport.uploadEnabled",false);
user_pref("datareporting.policy.dataSubmissionEnabled",false);
user_pref("network.prefetch-next",false);
user_pref("social.share.activationPanelEnabled",false);
