# Synopsis
The purpose of this patch is to get rid of a single quote url-encoding in HTTP requests.

Since version 3.0 ([commit](http://hg.mozilla.org/mozilla-central/rev/b3a950a7df1d)) Firefox started to url-encode a single quote into %27.

Sometimes such behavior can affect your ability to detect a SQL injection when, for example, $ENV["QUERY_STRING"] and $ENV["REQUEST_URI"] are used directly in SQL queries.

This patch allows you to modify the behavior of a single, double and back quotes.

# Installation
```bash
$ git clone https://nobody_@bitbucket.org/nobody_/ff_sexy.git
$ cd ff_sexy/
$ ./xpatch.sh
```
Remember that you have to patch libXUL everytime after Firefox update.

# Timeline of EscapeChars[] modification (which was breaking old patches)
// 2015-04-29 23:43 +0000 [Single Quotes should not be encoded in the path](http://hg.mozilla.org/mozilla-central/diff/db1bdd9d2464/xpcom/io/nsEscape.cpp#l1.13)

// 2015-04-17 19:45 +0000 [Backed out 1 changesets](http://hg.mozilla.org/mozilla-central/diff/255851401814/xpcom/io/nsEscape.cpp#l1.13)

// 2011-08-23 08:20 +0000 [Remove historic special handling for semicolons in URIs](http://hg.mozilla.org/mozilla-central/diff/1a09781a5480/xpcom/io/nsEscape.cpp#l1.13)

// 2008-03-20 16:49 +0000 [Single quotes in URLs should be escaped.](http://hg.mozilla.org/mozilla-central/diff/b3a950a7df1d/xpcom/io/nsEscape.cpp#l1.13)

# Additional
Put user.js to your Firefox Profile directory
