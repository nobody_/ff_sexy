#!/bin/bash
XUL="/usr/lib/firefox/libxul.so"
if [ ! -e "$XUL" ]; then
  echo "Please specify another path to libxul.so"
  exit 1
fi;

if [ "`pidof firefox iceweasel`" != "" ]; then
  echo "Please terminate firefox process"
  exit 1
fi;

if [ ! -e "./fix.c" ]; then
  echo "Where is ./fix.c?"
  git archive --remote=git@bitbucket.org:nobody_/ff_sexy.git HEAD -- fix.c | tar -x
  if [ $? -ne 0 ]; then
    echo "Download fix.c from here: https://bitbucket.org/nobody_/ff_sexy/src/"
    exit 1
  fi;
fi;

cc fix.c -Wall -Wextra -o fix

if [ `stat -c '%u' "$XUL"` -ne $EUID ]; then
  cp "$XUL" xul.so
  ./fix --all ./xul.so
  echo "Run as `stat -c '%U' \"${XUL}\"`: cp --no-preserve=all ./xul.so $XUL && chmod 755 $XUL"
else
  ./fix --all "$XUL"
fi;
