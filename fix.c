#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <getopt.h>

/* mozilla-release/xpcom/io/nsEscape.cpp:355 
 * I choose exactly this part of array cause i doubt developers would change values for alphanumerics
 * like they do for special characters
 */
static const uint32_t EscapeChars[16] =
//   0    1    2    3    4    5    6    7    8    9    A    B    C    D    E    F
{
//     0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  // 0x
//     0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  // 1x
//     0,1023,   0, 512,1023,   0,1023, 112,1023,1023,1023,1023,1023,1023, 953, 784,  // 2x   !"#$%&'()*+,-./
//  1023,1023,1023,1023,1023,1023,1023,1023,1023,1023,1008,1008,   0,1008,   0, 768,  // 3x  0123456789:;<=>?
  1008,1023,1023,1023,1023,1023,1023,1023,1023,1023,1023,1023,1023,1023,1023,1023     // 4x  @ABCDEFGHIJKLMNO
//  1023,1023,1023,1023,1023,1023,1023,1023,1023,1023,1023, 896, 896, 896, 896,1023,  // 5x  PQRSTUVWXYZ[\]^_
//     0,1023,1023,1023,1023,1023,1023,1023,1023,1023,1023,1023,1023,1023,1023,1023,  // 6x  `abcdefghijklmno
//  1023,1023,1023,1023,1023,1023,1023,1023,1023,1023,1023, 896,1012, 896,1023,   0,  // 7x  pqrstuvwxyz{|}~ DEL
//     0                                                                              // 80 to FF are zero
};
/* offset = (row * 16 * sizeof(uint32_t) + (column * sizeof(uint32_t)) */
static const off_t double_quote = 0x88;
static const off_t single_quote = 0x9c;
static const off_t back_quote   = 0x180;

static const uint32_t escape    = 0;
static const uint32_t unescape  = 1023;

void help(const char* const name) {
  fprintf(stderr, "Usage: %s [OPTION]... [FILE]\n", name);
  fprintf(stderr, "Patch XUL lib character escaping.\n\n");
  fprintf(stderr, "  --single\t%%27 -> '\n");
  fprintf(stderr, "  --double\t%%22 -> \"\n");
  fprintf(stderr, "  --back\t%%60 -> `\n");
  fprintf(stderr, "  --all\t\t[%%27, %%22, %%60] -> [',\",`]\n");
  fprintf(stderr, "  --check\tcurrent state\n");
}

off_t findOffset(const int fd) {
  uint32_t buf;
  uint8_t index = 0;
  off_t offset;

  while( read(fd, &buf, sizeof(buf)) == sizeof(buf) ) if( index == 16 )
    break;
  else if( buf == EscapeChars[index] )
    ++index;
  else
    index = 0;

  if( index == 0 )
    offset = -1;
  else
    offset = (lseek(fd, 0, SEEK_CUR) - 5 * sizeof(EscapeChars) - 4);

  return offset;
}

void patch_quote(const int fd, const off_t off, const char quote) {
  lseek(fd, off, SEEK_SET);
  write(fd, &unescape, sizeof(unescape));
  printf("(%c) patched\n", quote);
}

void check_quote(const int fd, const off_t off, const char quote) {
  uint32_t buf;
  lseek(fd, off, SEEK_SET);
  read(fd, &buf, sizeof(buf));
  if( buf != unescape )
    printf("(%c) not patched\n", quote);
  else
    printf("(%c) patched\n", quote);
}

int main(int argc, char **argv) {
  uint8_t patch_flag, check_flag;
  int index, opt, fd;
  off_t offset;
  const char* xul = NULL;
  
  static struct option long_opts[] = {
    {"single", no_argument, 0, 's' },
    {"double", no_argument, 0, 'd' },
    {"back",   no_argument, 0, 'b' },
    {"check",  no_argument, 0, 'c' },
    {"all",    no_argument, 0, 'a' },
    {"help",   no_argument, 0, 'h' },
    {0,        0,           0,  0  }
  };

  patch_flag = check_flag = 0;
  while((opt = getopt_long_only(argc, argv, "", long_opts, &index)) != -1 ) switch(opt) {
    case 's':
      patch_flag |= 1;
      break;
    case 'd':
      patch_flag |= 2;
      break;
    case 'b':
      patch_flag |= 4;
      break;
    case 'c':
      check_flag = 1;
      break;
    case 'a':
      patch_flag = 7;
      break;
    case 'h':
      help(argv[0]);
      exit(0);
      break;
    default:
      exit(0);
      break;
  }

  if( argc < 3 ) {
    help(argv[0]);
    exit(0);
  } 
  
  for( index = optind; index < argc; index++ ) if( xul != NULL ) {
    help(argv[0]);
    exit(0);
  } else {
    xul = argv[index];
  }

  /* TL;DR; */
  fd = open(xul, O_RDWR);
  if( fd == -1 ) {
    perror("open");
    return errno;
  }
  offset = findOffset(fd);

  if( offset == -1 ) {
    fprintf(stderr, "Invalid file\n");
    exit(0);
  }

  if( check_flag ) {
    check_quote(fd, offset + single_quote, '\'');
    check_quote(fd, offset + double_quote, '"' );
    check_quote(fd, offset + back_quote,   '`' );
    close(fd);
    exit(0);
  }

  if( patch_flag ) {
    if( patch_flag & 1 )
      patch_quote(fd, offset + single_quote, '\'');
    if( patch_flag & 2 )
      patch_quote(fd, offset + double_quote, '"' );
    if( patch_flag & 4 )
      patch_quote(fd, offset + back_quote,   '`' );
    close(fd);
    exit(0);
  }
  close(fd);
  return 0;
}
